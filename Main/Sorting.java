package ChestSorter.Main;

import java.util.ArrayList;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.ShulkerBox;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class Sorting
{
    public static void sortShulkerBox(ShulkerBox b)
    {
        sortInventory(b.getInventory());
    }

    public static void sortEnderChest(Player p)
    {
        sortInventory(p.getEnderChest());
    }

    public static void sortChest(Block c)
    {
        Chest c1 = (Chest)c.getState();
        sortInventory(c1.getInventory());
    }

    public static void sortPlayerInventory(Player p)
    {
        sortInventory(p.getInventory(), 9, 35);
    }

    public static void sortInventory(Inventory inv)
    {
        ArrayList<ItemStack> items = new ArrayList();
        items = getInventory(inv);

        items = getFullStackList(items);
        items = sortItems(items);
        items = sortItems(items);

        inv.clear();

        setInventory(inv, items);
    }

    public static void sortInventory(Inventory inv, int slot1, int slotn)
    {
        ArrayList<ItemStack> items = new ArrayList();
        items = getInventory(inv, 9, 35);

        items = getFullStackList(items);
        items = sortItems(items);
        items = sortItems(items);
        for (int i = slot1; i < slotn + 1; i++) {
            inv.clear(i);
        }
        setInventory(inv, items, slot1, slotn);
    }

    public static ArrayList<ItemStack> sortItems(ArrayList<ItemStack> is)
    {
        ArrayList<ItemStack> copy = (ArrayList)is.clone();

        ArrayList<ItemStack> items = new ArrayList();
        if (is.size() >= 1)
        {
            for (int x = 0; x < is.size(); x++)
            {
                int index = 0;
                for (int i = 0; i < copy.size(); i++)
                {
                    if (((ItemStack)copy.get(i)).getTypeId() < ((ItemStack)copy.get(index)).getTypeId())
                    {
                        index = i;
                        break;
                    }
                    if (((ItemStack)copy.get(i)).getType() == ((ItemStack)copy.get(index)).getType()) {
                        if (((ItemStack)copy.get(i)).getDurability() < ((ItemStack)copy.get(index)).getDurability()) {
                            index = i;
                        }
                    }
                }
                items.add(((ItemStack)copy.get(index)).clone());
                copy.remove(index);
            }
            if (copy.size() == 1) {
                items.add(((ItemStack)copy.get(0)).clone());
            }
        }
        return items;
    }

    public static ArrayList<ItemStack> getInventory(Inventory inv)
    {
        ArrayList<ItemStack> items = new ArrayList();
        for (int i = 0; i < inv.getSize(); i++) {
            if (inv.getItem(i) != null) {
                items.add(inv.getItem(i).clone());
            }
        }
        return items;
    }

    public static ArrayList<ItemStack> getInventory(Inventory inv, int slot1, int slotn)
    {
        ArrayList<ItemStack> items = new ArrayList();
        for (int i = slot1; i < slotn + 1; i++) {
            if (inv.getItem(i) != null) {
                items.add(inv.getItem(i));
            }
        }
        return items;
    }

    public static int getItemsSize(Player p)
    {
        int counter = 0;
        for (int i = 9; i < 36; i++) {
            if (p.getInventory().getItem(i) != null) {
                counter++;
            }
        }
        return counter;
    }

    public static void setInventory(Inventory inv, ArrayList<ItemStack> items)
    {
        for (int i = 0; i < items.size(); i++) {
            inv.setItem(i, (ItemStack)items.get(i));
        }
    }

    public static void setInventory(Inventory inv, ArrayList<ItemStack> items, int slot1, int slotn)
    {
        for (int i = 0; i < items.size(); i++) {
            inv.setItem(i + slot1, (ItemStack)items.get(i));
        }
    }

    public static ArrayList<ItemStack> getFullStackList(ArrayList<ItemStack> is)
    {
        ArrayList<ItemStack> copy = (ArrayList)is.clone();
        ArrayList<ItemStack> items = new ArrayList();
        if (is.size() > 1)
        {
            int types = 1;
            for (int x = 0; x < copy.size(); x++)
            {
                int amount = ((ItemStack)copy.get(x)).getAmount();
                ItemStack stack = ((ItemStack)copy.get(x)).clone();

                ArrayList<ItemStack> remove = new ArrayList();
                for (int y = 0; y < copy.size(); y++) {
                    if ((stack.getType().equals(((ItemStack)copy.get(y)).getType())) && (stack.getItemMeta().equals(((ItemStack)copy.get(y)).getItemMeta())) &&
                            (stack.getDurability() == ((ItemStack)copy.get(y)).getDurability()) && (x != y))
                    {
                        amount += ((ItemStack)copy.get(y)).getAmount();
                        remove.add((ItemStack)copy.get(y));
                    }
                }
                remove.add(stack.clone());
                for (int i = 0; i < remove.size(); i++) {
                    copy.remove(remove.get(i));
                }
                types++;
                x = 0;
            }
            copy = (ArrayList)is.clone();
            for (int a = 0; (a < types) && (copy.size() > 0); a++)
            {
                int amount = ((ItemStack)copy.get(0)).getAmount();
                ItemStack stack = ((ItemStack)copy.get(0)).clone();

                ArrayList<ItemStack> remove = new ArrayList();
                for (int y = 0; y < copy.size(); y++) {
                    if ((stack.getType().equals(((ItemStack)copy.get(y)).getType())) && (stack.getItemMeta().equals(((ItemStack)copy.get(y)).getItemMeta())) &&
                            (stack.getDurability() == ((ItemStack)copy.get(y)).getDurability()) && (y != 0))
                    {
                        amount += ((ItemStack)copy.get(y)).getAmount();
                        remove.add((ItemStack)copy.get(y));
                    }
                }
                remove.add(stack.clone());
                for (int i = 0; i < remove.size(); i++) {
                    copy.remove(remove.get(i));
                }
                for (;;)
                {
                    if (amount <= stack.getMaxStackSize())
                    {
                        ItemStack item = new ItemStack(stack.getType(), amount);
                        item.setDurability(stack.getDurability());
                        item.setItemMeta(stack.getItemMeta());
                        items.add(item);
                        break;
                    }
                    ItemStack item = new ItemStack(stack.getType(), stack.getMaxStackSize());
                    item.setDurability(stack.getDurability());
                    item.setItemMeta(stack.getItemMeta());
                    items.add(item);
                    amount -= stack.getMaxStackSize();
                }
            }
        }
        return items;
    }
}