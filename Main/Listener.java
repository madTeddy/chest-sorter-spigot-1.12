package ChestSorter.Main;

import ChestSorter.Events.SortingInventoryEvent;
import ChestSorter.Events.SortingNames;
import ChestSorter.Events.SortingTypes;
import ChestSorter.Lang.MessageTypes;
import ChestSorter.Lang.Messages;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.ShulkerBox;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

public class Listener implements org.bukkit.event.Listener
{
    @EventHandler
    public void onRightClick (PlayerInteractEvent e)
    {
        if (e.getHand() == null) return;
        if ((!Bukkit.getVersion().contains("1.8")) && ((!Main.itemBoolean) || (e.getHand().equals(EquipmentSlot.OFF_HAND)))) return;
        Player p = e.getPlayer();
        ItemStack item = p.getItemInHand().clone();
        item.setDurability((short)0);
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK)
        {
            if (item.equals(Main.item))
            {
                if (p.isSneaking()) {
                    SortPlayerInv(p, e);
                    return;
                }
                if ((e.getClickedBlock().getType() == Material.CHEST) || (e.getClickedBlock().getType() == Material.TRAPPED_CHEST))
                {
                    if (p.hasPermission("chestsorter.sort.item.chest"))
                    {
                        if (!Timer.PlayerCheck(p)) {
                            return;
                        }
                        SortingInventoryEvent e1 = new SortingInventoryEvent(p, SortingTypes.CLEANING_ITEM, e.getClickedBlock(), SortingNames.CHEST.getName());
                        Bukkit.getPluginManager().callEvent(e1);
                        if (!e1.isCancelled())
                        {
                            Sorting.sortChest(e.getClickedBlock());
                            ChangeDurability(p, e);
                            Messages.sendMessageToPlayer(MessageTypes.CHEST_SORTED, p);
                        }
                        return;
                    }
                    Messages.sendMessageToPlayer(MessageTypes.NO_PERMISSION, p);
                }
                else if (e.getClickedBlock().getType() == Material.ENDER_CHEST)
                {
                    if (p.hasPermission("chestsorter.sort.item.enderchest"))
                    {
                        if (!Timer.PlayerCheck(p)) {
                            return;
                        }
                        SortingInventoryEvent e1 = new SortingInventoryEvent(p, SortingTypes.CLEANING_ITEM, e.getClickedBlock(), SortingNames.ENDERCHEST.getName());
                        Bukkit.getPluginManager().callEvent(e1);
                        if (!e1.isCancelled())
                        {
                            Sorting.sortEnderChest(p);
                            ChangeDurability(p, e);
                            Messages.sendMessageToPlayer(MessageTypes.CHEST_SORTED, p);
                        }
                        return;
                    }
                    Messages.sendMessageToPlayer(MessageTypes.NO_PERMISSION, p);
                }
                else if (e.getClickedBlock().getType().name().toLowerCase().contains("shulker_box")) {
                    if (p.hasPermission("chestsorter.sort.item.shulkerbox"))
                    {
                        if (!Timer.PlayerCheck(p)) {
                            return;
                        }
                        ShulkerBox b = (ShulkerBox)e.getClickedBlock().getState();
                        SortingInventoryEvent e1 = new SortingInventoryEvent(p, SortingTypes.CLEANING_ITEM, e.getClickedBlock(), SortingNames.SHULKERBOX.getName());
                        Bukkit.getPluginManager().callEvent(e1);
                        if (!e1.isCancelled())
                        {
                            Sorting.sortShulkerBox(b);
                            ChangeDurability(p, e);
                            Messages.sendMessageToPlayer(MessageTypes.CHEST_SORTED, p);
                        }
                        return;
                    }
                    Messages.sendMessageToPlayer(MessageTypes.NO_PERMISSION, p);
                }
            }
        }
        else if (e.getAction() == Action.RIGHT_CLICK_AIR) {
            if (item.equals(Main.item)) {
                if (p.isSneaking()) {
                    SortPlayerInv(p, e);
                }
            }
        }
    }

    @EventHandler
    public void onPlayerDrop(PlayerDropItemEvent event)
    {
        ItemStack drop = event.getItemDrop().getItemStack();
        if (drop.equals(Main.item))
        {
            event.getItemDrop().remove();
            Messages.sendMessageToPlayer(MessageTypes.SORT_ITEM_DROP, event.getPlayer());
        }
    }

    private void ChangeDurability(Player player, PlayerInteractEvent interactEvent)
    {
        if (Main.durability) {
            player.getItemInHand().setDurability((short)(player.getItemInHand().getDurability() + 1));
            if (player.getItemInHand().getDurability() >= player.getItemInHand().getType().getMaxDurability()) {
                player.setItemInHand(null);
            }
        }
        interactEvent.setCancelled(true);
    }

    private void SortPlayerInv(Player p, PlayerInteractEvent e)
    {
        if (p.hasPermission("chestsorter.sort.playerinventory"))
        {
            if (!Timer.PlayerCheck(p)) {
                return;
            }
            SortingInventoryEvent e1 = new SortingInventoryEvent(p, SortingTypes.CLEANING_ITEM, null, SortingNames.PLAYER.getName());
            Bukkit.getPluginManager().callEvent(e1);
            if (!e1.isCancelled())
            {
                Sorting.sortPlayerInventory(p);
                ChangeDurability(p, e);
                Messages.sendMessageToPlayer(MessageTypes.INVENTORY_SORTED, p);
            }
            return;
        }
        Messages.sendMessageToPlayer(MessageTypes.NO_PERMISSION, p);
    }
}