package ChestSorter.Main;

import ChestSorter.Lang.MessageTypes;
import ChestSorter.Lang.Messages;
import java.util.ArrayList;
import java.util.Iterator;
import org.bukkit.entity.Player;

public class Timer
{
    private Player p;
    private int time;

    public Timer(Player p, int time)
    {
        this.p = p;
        this.time = time;
    }

    public Player getPlayer()
    {
        return this.p;
    }

    public int getTime()
    {
        return this.time;
    }

    public void setTime(int time)
    {
        this.time = time;
    }

    static ArrayList<Timer> times = new ArrayList();

    public static void update()
    {
        if (Main.timer)
        {
            Timer t;
            for (Iterator localIterator1 = times.iterator(); localIterator1.hasNext(); t.setTime(t.getTime() - 1)) {
                t = (Timer)localIterator1.next();
            }
            ArrayList<Integer> remove = new ArrayList();
            for (int i = 0; i < times.size(); i++) {
                if (((Timer)times.get(i)).getTime() <= 0) {
                    remove.add(Integer.valueOf(i));
                }
            }
            int i;
            for (Iterator localIterator2 = remove.iterator(); localIterator2.hasNext(); times.remove(i)) {
                i = ((Integer)localIterator2.next()).intValue();
            }
        }
    }

    public static boolean isPlayerOnList(Player p)
    {
        for (Timer t : times) {
            if (t.getPlayer().equals(p)) {
                return true;
            }
        }
        return false;
    }

    public static int getPlayerTime(Player p)
    {
        for (Timer t : times) {
            if (p.equals(p)) {
                return t.getTime();
            }
        }
        return 0;
    }

    public static void setPlayerOnList(Player p)
    {
        times.add(new Timer(p, Main.time));
    }

    public static boolean PlayerCheck(Player p)
    {
        if (Main.timer)
        {
            if (isPlayerOnList(p))
            {
                Messages.sendMessageToPlayer(MessageTypes.SORTING_ON_COOLDOWN, p);
                return false;
            }
            setPlayerOnList(p);
            return true;
        }
        return true;
    }
}
