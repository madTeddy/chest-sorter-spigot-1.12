package ChestSorter.Main.CMD;

import ChestSorter.Config.Config;
import ChestSorter.Lang.MessageTypes;
import ChestSorter.Lang.Messages;
import ChestSorter.Main.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CMD_ChestSorter implements CommandExecutor {

    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args)
    {
        if (!(cs instanceof Player)){
            cs.sendMessage(ChatColor.RED + "Вы не можете использовать это через консоль!");
            return true;
        }
        Player p = (Player)cs;
        String command = args.length > 0 ? args[0].toLowerCase() : "";
        String cmdArgument = args.length > 1 ? args[1].toLowerCase() : "";
        switch (command)
        {
            case "debugmsg":
                if (!p.hasPermission("chestsorter.debugmsg")){
                    Messages.sendMessageToPlayer(MessageTypes.NO_PERMISSION, p);
                    return true;
                }
                if (args.length != 2 || !cmdArgument.equals("true") && !cmdArgument.equals("false")){
                    p.sendMessage("§cПример: /chestsorter debugmsg <true/false>");
                    return true;
                }
                Boolean debugMsg = Boolean.valueOf(cmdArgument);
                Config.setDebugMsg(debugMsg);
                Main.debugMsg = debugMsg;
                p.sendMessage("§aДебаг сообщений: " + debugMsg);
            break;
            case "version":
                p.sendMessage("Плагин: §aChestSorter\nВерсия: §a1.0.5\nАвтор: §cmadTeddy");
            break;
            default:
                p.sendMessage("§cПример: /chestsorter <debugmsg/version> <arg>");
            break;
        }
        return true;
    }
}