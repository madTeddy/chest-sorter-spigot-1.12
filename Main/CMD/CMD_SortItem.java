package ChestSorter.Main.CMD;

import ChestSorter.Main.Main;
import ChestSorter.Lang.MessageTypes;
import ChestSorter.Lang.Messages;
import ChestSorter.Config.Config;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CMD_SortItem implements CommandExecutor
{
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args)
    {
        if (!(cs instanceof Player)){
            cs.sendMessage(ChatColor.RED + "Вы не можете использовать это через консоль!");
            return true;
        }
        Player p = (Player)cs;
        if (args.length > 1) {
            if (args[0].equalsIgnoreCase("rename"))
            {
                if (p.hasPermission("chestsorter.config"))
                {
                    String newname = null;
                    for (int i = 1; i < args.length; i++) {
                        if (i == 1) {
                            newname = args[1];
                        } else {
                            newname = newname + " " + args[i];
                        }
                    }
                    newname = newname.replace("&", "§");
                    p.sendMessage("§aИмя вашего нового сортировщика: " + newname);
                    ItemStack is = Main.item;
                    ItemMeta im = is.getItemMeta();
                    im.setDisplayName(newname);
                    Main.item.setItemMeta(im);
                    Config.setItem(Main.item);
                    if (args.length == 1) {
                        return true;
                    }
                }
                else
                {
                    Messages.sendMessageToPlayer(MessageTypes.NO_PERMISSION, p);
                    return true;
                }
                return true;
            }
        }
        if (args.length == 1)
        {
            if (args[0].equalsIgnoreCase("rename"))
            {
                p.sendMessage("§cПример: /sortitem rename <name>");
                return true;
            }
            if (args[0].equalsIgnoreCase("setItem"))
            {
                if (p.hasPermission("chestsorter.config"))
                {
                    ItemStack item = p.getInventory().getItemInMainHand().clone();
                    if (item != null)
                    {
                        item.setDurability((short)0);
                        item.setAmount(1);
                        Config.setItem(item);
                        Main.item = item;
                        p.sendMessage("§a" + item + " теперь новый сортировщик.");
                        return true;
                    }
                    p.sendMessage("§cВы должны держать вещь, чтобы сделать это.");
                    return true;
                }
                Messages.sendMessageToPlayer(MessageTypes.NO_PERMISSION, p);
                return true;
            }
            if (args[0].equalsIgnoreCase("get"))
            {
                if (p.hasPermission("chestsorter.item.get"))
                {
                    if (!p.getInventory().contains(Main.item))
                    {
                        p.getInventory().addItem(Main.item);
                        p.sendMessage("§aВы взяли сортировщик. Нажмите правой кнопкой мыши по сундуку, для его сортировки.");
                        return true;
                    }
                    Messages.sendMessageToPlayer(MessageTypes.SORT_ITEM_EXISTS, p);
                    return true;
                }
                Messages.sendMessageToPlayer(MessageTypes.NO_PERMISSION, p);
                return true;
            }
            p.sendMessage("§cПример: /sortitem <setactive/durability/get/give/rename> <arg>");
            return true;
        }
        else
        {
            if (args.length == 2)
            {
                if (args[0].equalsIgnoreCase("setActive"))
                {
                    if (p.hasPermission("chestsorter.config"))
                    {
                        if ((args[1].equalsIgnoreCase("true")) || (args[1].equalsIgnoreCase("false")))
                        {
                            boolean b = false;
                            if (args[1].equalsIgnoreCase("true")) {
                                b = true;
                            }
                            Config.setItemBoolean(b);
                            Main.itemBoolean = b;
                            p.sendMessage("§aСортировщик: " + b);
                            return true;
                        }
                        p.sendMessage("§cПример: /cleanitem setactive <true/false>");
                        return true;
                    }
                    Messages.sendMessageToPlayer(MessageTypes.NO_PERMISSION, p);
                    return true;
                }
                if (args[0].equalsIgnoreCase("Durability"))
                {
                    if (p.hasPermission("chestsorter.config"))
                    {
                        if ((args[1].equalsIgnoreCase("true")) || (args[1].equalsIgnoreCase("false")))
                        {
                            boolean b = false;
                            if (args[1].equalsIgnoreCase("true")) {
                                b = true;
                            }
                            Config.setDurabilityBoolean(b);
                            Main.durability = b;
                            p.sendMessage("§aДолговечность: " + b);
                            return true;
                        }
                        p.sendMessage("§cПример: /sortitem durability<true/false>");
                        return true;
                    }
                    Messages.sendMessageToPlayer(MessageTypes.NO_PERMISSION, p);
                    return true;
                }
                if (args[0].equalsIgnoreCase("give"))
                {
                    if (p.hasPermission("chestsorter.item.give"))
                    {
                        Player p2 = Bukkit.getPlayer(args[1]);
                        if (p2 != null)
                        {
                            p2.getInventory().addItem(new ItemStack[] { Main.item });
                            p.sendMessage("§aИгрок " + p.getName() + " взял сортировщик.");
                            return true;
                        }
                        p.sendMessage("§cИгрок " + args[1] + " оффлайн.");
                        return true;
                    }
                    Messages.sendMessageToPlayer(MessageTypes.NO_PERMISSION, p);
                    return true;
                }
                p.sendMessage("§cПример: /sortitem <setactive/durability/get/give/rename> <arg>");
                return true;
            }
            p.sendMessage("§cПример: /sortitem <setactive/durability/get/give/rename> <arg>");
            return true;
        }
    }
}