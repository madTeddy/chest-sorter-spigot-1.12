package ChestSorter.Main.CMD;

import ChestSorter.Lang.MessageTypes;
import ChestSorter.Lang.Messages;
import ChestSorter.Events.SortingInventoryEvent;
import ChestSorter.Events.SortingNames;
import ChestSorter.Events.SortingTypes;
import ChestSorter.Main.Sorting;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.ShulkerBox;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.BlockIterator;

public class CMD_SortChest implements CommandExecutor
{
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args)
    {
        if (!(cs instanceof Player)){
            cs.sendMessage(ChatColor.RED + "Вы не можете использовать это через консоль!");
            return true;
        }
        Player p = (Player)cs;
        if (args.length == 0)
        {
            BlockIterator iterator = new BlockIterator(p, 100);

            Block lastBlock = iterator.next();
            while (iterator.hasNext())
            {
                lastBlock = iterator.next();
                if (lastBlock.getType() != Material.AIR) {
                    break;
                }
            }
            Location loc = lastBlock.getLocation();

            Block chest = loc.getBlock();
            if ((chest.getType() == Material.CHEST) || (chest.getType() == Material.TRAPPED_CHEST))
            {
                if (p.hasPermission("chestsorter.sort.chest"))
                {
                    SortingInventoryEvent e = new SortingInventoryEvent(p, SortingTypes.COMMAND, chest, SortingNames.CHEST.getName());
                    Bukkit.getPluginManager().callEvent(e);
                    if (!e.isCancelled()) {
                        Sorting.sortChest(chest);
                        Messages.sendMessageToPlayer(MessageTypes.CHEST_SORTED, p);
                    }
                    return true;
                }
                Messages.sendMessageToPlayer(MessageTypes.NO_PERMISSION, p);
                return true;
            }
            if (chest.getType() == Material.ENDER_CHEST)
            {
                if (p.hasPermission("chestsorter.sort.enderchest"))
                {
                    SortingInventoryEvent e = new SortingInventoryEvent(p, SortingTypes.COMMAND, chest, SortingNames.ENDERCHEST.getName());
                    Bukkit.getPluginManager().callEvent(e);
                    if (!e.isCancelled()) {
                        Sorting.sortEnderChest(p);
                        Messages.sendMessageToPlayer(MessageTypes.CHEST_SORTED, p);
                    }
                    return true;
                }
                Messages.sendMessageToPlayer(MessageTypes.NO_PERMISSION, p);
                return true;
            }
            if (chest.getType().name().toLowerCase().contains("shulker_box"))
            {
                if (p.hasPermission("chestsorter.sort.shulkerbox"))
                {
                    SortingInventoryEvent e = new SortingInventoryEvent(p, SortingTypes.COMMAND, chest, SortingNames.SHULKERBOX.getName());
                    Bukkit.getPluginManager().callEvent(e);
                    if (!e.isCancelled())
                    {
                        ShulkerBox b = (ShulkerBox)chest.getState();
                        Sorting.sortShulkerBox(b);
                        Messages.sendMessageToPlayer(MessageTypes.CHEST_SORTED, p);
                    }
                    return true;
                }
                Messages.sendMessageToPlayer(MessageTypes.NO_PERMISSION, p);
                return true;
            }
            p.sendMessage("§cБлок на который вы смотрите не является сундуком!");
            return true;
        }
        p.sendMessage("§cПример: /sortchest");
        return true;
    }

    public final Block getTargetBlock(Player player, int range)
    {
        BlockIterator iterator = new BlockIterator(player, range);
        Block lastBlock = iterator.next();
        while (iterator.hasNext())
        {
            lastBlock = iterator.next();
            if (lastBlock.getType() != Material.AIR) {
                break;
            }
        }
        return lastBlock;
    }
}