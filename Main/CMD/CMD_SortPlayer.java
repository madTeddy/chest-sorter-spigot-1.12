package ChestSorter.Main.CMD;

import ChestSorter.Lang.MessageTypes;
import ChestSorter.Lang.Messages;
import ChestSorter.Events.SortingInventoryEvent;
import ChestSorter.Events.SortingNames;
import ChestSorter.Events.SortingTypes;
import ChestSorter.Main.Sorting;
import ChestSorter.Main.Timer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CMD_SortPlayer implements CommandExecutor
{
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args)
    {
        if (!(cs instanceof Player)){
            cs.sendMessage(ChatColor.RED + "Вы не можете использовать это через консоль!");
            return true;
        }
        Player p = (Player)cs;
        if (p.hasPermission("chestsorter.sort.playerinventory"))
        {
            if (args.length == 0) {
                if (Sorting.getItemsSize(p) > 1)
                {
                    if (!Timer.PlayerCheck(p)) {
                        return true;
                    }
                    SortingInventoryEvent e = new SortingInventoryEvent(p, SortingTypes.COMMAND, null, SortingNames.PLAYER.getName());
                    Bukkit.getPluginManager().callEvent(e);
                    if (!e.isCancelled()) {
                        Sorting.sortPlayerInventory(p);
                        Messages.sendMessageToPlayer(MessageTypes.INVENTORY_SORTED, p);
                    }
                    return true;
                }
            }
        }
        else
        {
            Messages.sendMessageToPlayer(MessageTypes.NO_PERMISSION, p);
            return true;
        }
        return true;
    }
}