package ChestSorter.Main.CMD;

import ChestSorter.Main.Main;
import ChestSorter.Lang.MessageTypes;
import ChestSorter.Lang.Messages;
import ChestSorter.Config.Config;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CMD_Timer implements CommandExecutor
{
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args)
    {
        if (!(cs instanceof Player)){
            cs.sendMessage(ChatColor.RED + "Вы не можете использовать это через консоль!");
            return true;
        }
        Player p = (Player)cs;
        if (p.hasPermission("chestsorter.config"))
        {
            if (args.length == 2)
            {
                if (args[0].equalsIgnoreCase("setActive"))
                {
                    if (args[1].equalsIgnoreCase("true"))
                    {
                        if (!Main.timer)
                        {
                            Config.setTimer(true);
                            Main.timer = true;
                        }
                        p.sendMessage("§aТаймер: включен");
                        return true;
                    }
                    if (args[1].equalsIgnoreCase("false"))
                    {
                        if (Main.timer)
                        {
                            Config.setTimer(false);
                            Main.timer = false;
                        }
                        p.sendMessage("§aТаймер: выключен");
                        return true;
                    }
                    p.sendMessage("§cПример: /timer <setActive> <true/false>");
                    return true;
                }
                if (args[0].equalsIgnoreCase("setTime"))
                {
                    int time = Integer.valueOf(args[1]).intValue();
                    if (Main.time != time)
                    {
                        Main.time = time;
                        Config.setTime(time);
                    }
                    p.sendMessage("§aВремя: " + time);
                    return true;
                }
                p.sendMessage("§cПример: /timer <setActive/setTime>");
                return true;
            }
            p.sendMessage("§cПример: /timer <setActive/setTime>");
            return true;
        }
        Messages.sendMessageToPlayer(MessageTypes.NO_PERMISSION, p);
        return true;
    }
}