package ChestSorter.Main;

import ChestSorter.Main.CMD.*;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import ChestSorter.Lang.Messages;
import ChestSorter.Config.Config;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Main extends JavaPlugin
{

    public static boolean itemBoolean = true;
    public static ItemStack item = new ItemStack(Material.STICK);
    public static boolean durability = true;
    public static boolean timer = false;
    public static int time = 5;
    public static boolean debugMsg = false;
    private Counter c = new Counter();

    @Override
    public void onEnable()
    {
        getLogger().info("Chest Sorter was enabled!");
        loadConfig();

        getCommand("sortchest").setExecutor(new CMD_SortChest());
        getCommand("sortplayer").setExecutor(new CMD_SortPlayer());
        getCommand("sortitem").setExecutor(new CMD_SortItem());
        getCommand("timer").setExecutor(new CMD_Timer());
        getCommand("chestsorter").setExecutor(new CMD_ChestSorter());

        Bukkit.getPluginManager().registerEvents(new Listener(), this);

        c.start();
    }
    @Override
    public void onDisable()
    {
        getLogger().info("Chest Sorter was disabled!");
    }

    private void loadConfig()
    {
        Config.save();

        Messages.loadStrings();
        Messages.saveStrings(Messages.getList());
        item = Config.getItem();
        if (Config.getItem() == null)
        {
            item = new ItemStack(Material.STICK);
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("§aСортировщик");
            item.setItemMeta(meta);
            Config.setItem(item);
        }
        if (!Config.containsItemBoolean()) {
            Config.setItemBoolean(true);
        } else {
            itemBoolean = Config.getItemBoolean();
        }
        if (!Config.containsDurabilityBoolean()) {
            Config.setDurabilityBoolean(true);
        } else {
            durability = Config.getDurabilityBoolean();
        }
        if (!Config.containsTimer()) {
            Config.setTimer(false);
        } else {
            timer = Config.getTimer();
        }
        if (!Config.containsTime()) {
            Config.setTime(time);
        } else {
            time = Config.getTime();
        }
        if (!Config.containsDebugMsg()){
            Config.setDebugMsg(false);
        }
        else {
            debugMsg = Config.getDebugMsg();
        }
    }
}