package ChestSorter.Events;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class SortingInventoryEvent extends Event implements Cancellable
{
    public static HandlerList handlers = new HandlerList();
    boolean cancelled = false;
    Player p;
    SortingTypes it;
    Block b;
    String name;

    public SortingInventoryEvent(Player p, SortingTypes it, Block b, String name)
    {
        this.p = p;
        this.it = it;
        this.b = b;
        this.name = name;
    }

    public Player getPlayer()
    {
        return this.p;
    }

    public SortingTypes getSortingType()
    {
        return this.it;
    }

    public String getName()
    {
        return this.name;
    }

    public Block getBlock()
    {
        return this.b;
    }

    public boolean isCancelled()
    {
        return this.cancelled;
    }

    public void setCancelled(boolean c)
    {
        this.cancelled = c;
    }

    public HandlerList getHandlers()
    {
        return handlers;
    }
}