package ChestSorter.Events;

public enum SortingNames
{
    ENDERCHEST("sorting_enderchest"),  SHULKERBOX("sorting_shulkerbox"),  CHEST("sorting_chest"),  PLAYER("sorting_player");

    private String name;

    SortingNames(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return this.name;
    }
}