package ChestSorter.Config;

import java.io.File;
import java.io.IOException;
import java.util.List;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

public class Config
{
    public static File ConfigFile = new File("plugins/ChestSorter", "config.yml");
    public static FileConfiguration Config = YamlConfiguration.loadConfiguration(ConfigFile);

    public static void save()
    {
        try
        {
            Config.save(ConfigFile);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static void setMessages(List<String> list)
    {
        Config.set("messages", list);
        save();
    }

    public static List<String> getMessages()
    {
        return Config.getStringList("messages");
    }

    public static void setItem(ItemStack is)
    {
        Config.set("sortingItem", is);
        save();
    }
    public static ItemStack getItem()
    {
        return Config.getItemStack("sortingItem");
    }
    public static void setItemBoolean(boolean b)
    {
        Config.set("active", Boolean.valueOf(b));
        save();
    }
    public static boolean getItemBoolean()
    {
        return Config.getBoolean("active");
    }
    public static boolean containsItemBoolean() { return Config.contains("active"); }

    public static void setDurabilityBoolean(boolean b)
    {
        Config.set("durability", Boolean.valueOf(b));
        save();
    }
    public static boolean getDurabilityBoolean()
    {
        return Config.getBoolean("durability");
    }
    public static boolean containsDurabilityBoolean() { return Config.contains("durability"); }

    public static boolean containsTimer() { return Config.contains("timer"); }
    public static void setTimer(boolean timer)
    {
        Config.set("timer", Boolean.valueOf(timer));
        save();
    }
    public static boolean getTimer()
    {
        return Config.getBoolean("timer");
    }

    public static boolean containsTime() { return Config.contains("time"); }
    public static void setTime(int time)
    {
        Config.set("time", Integer.valueOf(time));
        save();
    }
    public static int getTime()
    {
        return Config.getInt("time");
    }

    public static void setDebugMsg(boolean debug)
    {
        Config.set("debugMsg", debug);
        save();
    }
    public static boolean getDebugMsg() { return Config.getBoolean("debugMsg"); }
    public static boolean containsDebugMsg() { return Config.contains("debugMsg"); }
}