package ChestSorter.Lang;

import ChestSorter.Main.Main;
import ChestSorter.Config.Config;
import ChestSorter.Main.Timer;
import java.util.ArrayList;

import org.bukkit.entity.Player;

public class Messages
{
    private static final String no_permission = "&cИзвините, у вас нет доступа к этой команде.";
    private static final String sorting_on_cooldown = "&cВы сможете отсортировать свой инвентарь через &4%cooldown% сек.";
    private static final String inventory_sorted = "&aИнвентарь отсортирован!";
    private static final String chest_sorted = "&aСундук отсортирован!";
    private static final String sort_item_drop = "&aСортировщик был удалён!";
    private static final String sort_item_exists = "&cУ вас уже есть сортировщик!";
    private static ArrayList<String> messages = new ArrayList();

    public static void sendMessageToPlayer(MessageTypes mt, Player p)
    {
        String message = null;
        switch (mt)
        {
            case INVENTORY_SORTED:
                message = getMessage(messages.get(0), p);
                break;
            case NO_PERMISSION:
                message = getMessage(messages.get(1), p);
                break;
            case SORTING_ON_COOLDOWN:
                message = getMessage(messages.get(2), p);
                break;
            case CHEST_SORTED:
                message = getMessage(messages.get(3), p);
            break;
            case SORT_ITEM_DROP:
                message = getMessage(messages.get(4), p);
            break;
            case SORT_ITEM_EXISTS:
                message = getMessage(messages.get(5), p);
            break;
        }
        p.sendMessage(message);
        if (Main.debugMsg){
            Exception ex = new Exception();
            StackTraceElement[] trace = ex.getStackTrace();
            System.out.println("Message: " + message);
            for (StackTraceElement element : trace){
                System.out.println("Class: " + element.getClassName() + " called on line: " + element.getLineNumber());
            }
        }
    }

    public static String getMessage(String msg, Player p)
    {
        String newmsg = msg;

        newmsg = newmsg.replaceAll("%player%", p.getDisplayName());
        newmsg = newmsg.replaceAll("%cooldown%", String.valueOf(Timer.getPlayerTime(p)));
        newmsg = newmsg.replaceAll("&", "§");

        return newmsg;
    }

    public static void loadStrings()
    {
        messages.add(0, inventory_sorted);
        messages.add(1, no_permission);
        messages.add(2, sorting_on_cooldown);
        messages.add(3, chest_sorted);
        messages.add(4, sort_item_drop);
        messages.add(5, sort_item_exists);
        ArrayList<String> newmessages = (ArrayList)Config.getMessages();
        for (int i = 0; i < newmessages.size(); i++) {
            messages.set(i, (String)newmessages.get(i));
        }
    }

    public static void saveStrings(ArrayList<String> msgs)
    {
        Config.setMessages(msgs);
    }

    public static ArrayList<String> getList()
    {
        return messages;
    }
}